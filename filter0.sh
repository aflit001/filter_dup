#!/bin/bash

READ1=$1
READ2=$2
MNAME=${READ1}_${READ2}

#http://compbionovice.blogspot.nl/2012/10/i-recently-ran-into-issue-with-assembly.html
#####Prepare fastq files by removing qual scores and placing the sequence as a 2nd column to the IDs.
if [[ ! -f "$READ1.noqual.txt" ]]; then
	echo "converting $READ1 to fasta"
	grep /1$ -A 1 $READ1 | sed '/--/d' | awk '/[0-9]$/{printf $0" ";next;}1' | sed 's#/1##g' > $READ1.noqual.txt
fi

if [[ ! -f "$READ2.noqual.txt" ]]; then
	echo "converting $READ2 to fasta"
	grep /2$ -A 1 $READ2 | sed '/--/d' | awk '/[0-9]$/{printf $0" ";next;}1' | sed 's#/2##g' > $READ2.noqual.txt
fi

#####Merge sequences and remove duplicates.
if [[ ! -f "${MNAME}_rmdup.txt" ]]; then
	echo "merging sequences"
	awk '{p=$2; getline<f} !A[$2=p $2]++' f=$READ2.noqual.txt $READ1.noqual.txt | tr " " "\t" > ${MNAME}_rmdup.txt
fi

#####Prepare read IDs to be used for searching.
if [[ ! -f "${MNAME}_rmdup_IDs.txt" ]]; then
	echo "getting ids"
	cut -f1 ${MNAME}_rmdup.txt > ${MNAME}_rmdup_IDs.txt
fi

if [[ ! -f "${READ1}_rmdup_IDs.txt" ]]; then
	echo "creating forbidden ids for $READ1"
	sed 's#$#/1#g' ${MNAME}_rmdup_IDs.txt > ${READ1}_rmdup_IDs.txt
fi

if [[ ! -f "${READ2}_rmdup_IDs.txt" ]]; then
	echo "creating forbidden ids for $READ2"
	sed 's#$#/2#g' ${MNAME}_rmdup_IDs.txt > ${READ2}_rmdup_IDs.txt
fi

#####Get non-duplicate fastq reads.
if [[ ! -f "${READ1}_rmdup.fastq" ]]; then
	echo "filtering $READ1"
	awk 'NR==FNR{A[$1]=$1;next} $1 in A{print;getline;print;getline;print;getline;print}' FS="\t" ${READ1}_rmdup_IDs.txt FS="\t" OFS="\t" ${READ1} > ${READ1}_rmdup.fastq
fi

if [[ ! -f "${READ2}_rmdup.fastq" ]]; then
	echo "filtering $READ2"
	awk 'NR==FNR{A[$1]=$1;next} $1 in A{print;getline;print;getline;print;getline;print}' FS="\t" ${READ2}_rmdup_IDs.txt FS="\t" OFS="\t" ${READ2} > ${READ2}_rmdup.fastq
fi

echo "DONE"
