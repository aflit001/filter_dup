set -xeu

FASTQ1=$1
FASTQ2=$2

FASTQ2RC=$FASTQ2.rc.fastq

echo "FASTQ 1  $FASTQ1"
echo "FASTQ 2  $FASTQ2"
echo "FASTQ RC $FASTQ2RC"
echo

ALL=out/all
VALID=out/valid
INVALID=out/invalid

if [[ ! -f "$FASTQ1" ]]; then
	echo "NO INPUT FASTQ 1"
	exit 1
fi
if [[ ! -f "$FASTQ2" ]]; then
	echo "NO INPUT FASTQ 2"
	exit 1
fi

if [[ ! -f "$ALL" ]]; then
	echo "GETTING ALL"
	grep "^@" $FASTQ1 $FASTQ2 | sed 's#/[1,2]##g' | sort > $ALL
fi

#echo "GETTING RC"
#if [[ ! -f "FASTQ2RC" ]]; then
#	cat $FASTQ2 | fastqreversecomp.py > $FASTQ2RC
#fi

echo "FILTERING"
if [[ ! -f "$VALID" ]]; then
	scripts/run_assembly_removeDuplicateReads.pl $FASTQ1 $FASTQ2 | grep "^@" | sed 's#/[1,2]##g' | sort > $VALID
fi

echo "GENERATING INVALID LIST"
sort $ALL $VALID | uniq -u > $INVALID

echo
echo "ALL     : "`wc -l $ALL`
echo "VALID   : "`wc -l $VALID`
echo "INVALID : "`wc -l $INVALID`

echo

echo "DONE"
