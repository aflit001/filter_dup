#!/usr/bin/python
import os, sys
import string
import time
import re
import argparse
import hashlib
import gzip
import traceback
import functools

import multiprocessing
from multiprocessing.reduction import reduce_connection
from multiprocessing           import Pipe, Pool, Queue
#from multiprocessing.managers import BaseManager
#import subprocess
#import pickle
from itertools import islice

#./filter_dups.py --homo -n 50 -i 8000.dedup.sff

#class QueueManager(BaseManager): pass

man = multiprocessing.Manager()
ns  = man.Namespace()

printevery = 100000
bufferSize = 64 * 1024*1024

trans      = string.maketrans(r'ACGTN', r'TGCAN')
rep1       = re.compile(r'/1| 1:\D:\d:\D*$')
rep2       = re.compile(r'/2| 2:\D:\d:\D*$')
rehomo1    = re.compile(r'(.)\1+?')
rehomo2    = r'\1'

#seqs       = set()
#dups       = []
#uniqs      = []
#regcount   = 0
#start      = time.time()

DEBUG           = True
DEBUG_NUM_READS = 500000

def openfile(infile, method, buff=bufferSize, compresslevel=1):
    fhd = None

    if infile.endswith('.gz'):
        fhd = gzip.open(infile, method+'b', compresslevel)

    else:
        fhd = open(infile, method, buff)

    return fhd



def sff2fq(filename, outbasename):
    cmd  = "sff_extract -c -Q --out_basename=%s %s" % (outbasename, filename)
    print "extracting sff:",cmd
    res = subprocess.call(cmd, shell=True)
    if res:
        print "error running sff extract"
        sys.exit(1)

    return outbasename + '.fastq'



def sffFilter(fq, sff):
    #.rmdup.excludedIds
    fqids  = fq + '.rmdup.excludedIds'
    sffout = fq + '.sff'
    cmd    = "sfffile -o %s -e %s %s" % (sffout, fqids, sff)
    print 'filtering sff:', cmd

    res = subprocess.call(cmd, shell=True)

    if res:
        print "error running sff extract"
        sys.exit(1)

    pass



class filehandler(object):
    numLines = 10000
    def __init__(self, filename, listl):
        self.filename     = filename
        self.list         = listl

        self.linesList    = []

    def write(self, line):
        if len( self.linesList ) < self.numLines:
            self.linesList.append( line )

        else:
            self.list.extend( self.linesList )
            self.linesList = [ line ]

    def writelines(self, lines):
        if len( self.linesList ) < self.numLines:
            self.linesList += lines
        else:
            self.list      += self.linesList
            self.linesList  = lines

    def close(self):
        print "closing", self.filename
        if len( self.linesList ) > 0: self.list.extend( self.linesList )
        self.linesList = []


#
#class setController(object):
#    def __init__(self):
#        self._sets = []
#
#    def add(self, lstl=man.list()):
#        setl = set()
#        self._sets.append( [setl, lstl] )
#
#        return setWrapper( lstl )
#
#    def _check(self):
#        for sets in self._sets:
#            setl, listl = sets
#
#            if len(listl) > 0:
#                for el in listl:
#                    setl.add( el )



class setWrapper(object):
    def __init__(self, lstl):
        self.lst    = lstl

    def add(self, data):
        self.lst.append( data )

    def update(self, data):
        self.lst.extend( data )



class seqdb(object):
    def __init__(self,  fn1, fn1o, fn1u, fhd1o, fhd1u,
                        fn2, fn2o, fn2u, fhd2o, fhd2u,
                        report_fn      , report_fhd   ,
                        excluded_fn    , excluded_fhd ,
                        uniques_fn     , uniques_fhd  ,

                        countstr="",
                        pairnum=None   , numpairs=None,
                        filenum=None   , numfiles=None,
                        paired=False   , onlyid=False ,

                        seqsl=set(),
                        dupsl=list(),
                        uniqsl=list()):

        self.fn1          = fn1
        self.fn2          = fn2

        self.fn1o         = fn1o
        self.fn2u         = fn2u

        self.fhd1o        = fhd1o
        self.fhd1u        = fhd1u

        self.fhd2o        = fhd2o
        self.fhd2u        = fhd2u

        self.report_fn    = report_fn
        self.report_fhd   = report_fhd

        self.excluded_fn  = excluded_fn
        self.excluded_fhd = excluded_fhd

        self.uniques_fn   = uniques_fn
        self.uniques_fhd  = uniques_fhd

        self.countstr     = countstr

        self.pairnum      = pairnum
        self.numpairs     = numpairs

        self.filenum      = filenum
        self.numfiles     = numfiles

        self.paired       = paired
        self.onlyid       = onlyid

        self.seqs         = seqsl
        self.dups         = dupsl
        self.uniqs        = uniqsl

        self.regcount     = 0
        self.lastcount    = 0
        self.lastReg      = 0
        self.lasttime     = time.time()
        self.start        = time.time()


    def addBad(self, header_name1, seq1, reg1, header_name2, seq2, reg2):
        #print "adding good:"
        #print header_name1, seq1, reg1, header_name2, seq2, reg2

        self.seqs.update(      set([seq1, seq2]) )
        self.dups.append(      header_name1 )

        if not self.onlyid:
            self.fhd1u.writelines( reg1         )

            if self.paired:
                self.fhd2u.writelines( reg2         )


    def addGood(self, header_name1, seq1, reg1, header_name2, seq2, reg2):
        #print "adding good:"
        #print header_name1, seq1, reg1, header_name2, seq2, reg2

        self.seqs.update(  set([seq1, seq2]) )
        self.uniqs.append( header_name1 )

        if not self.onlyid:
            self.fhd1o.writelines( reg1   )
            self.fhd2o.writelines( reg2   )


    def add(self, header_name1, seq1, reg1, header_name2, seq2, reg2, bad=False):
        # check if sequence was already present
        self.regcount += 1
        self.printProgress()

        if bad:
            #sys.stdout.write('x')
            self.addBad(header_name1, seq1, reg1, header_name2, seq2, reg2)
            return


        if seq1 in self.seqs:
            #sys.stdout.write('b')
            self.addBad(header_name1, seq1, reg1, header_name2, seq2, reg2)
            return

        #check reverse strand
        if self.paired:
            if seq2 in self.seqs:
                #sys.stdout.write('B')
                self.addBad(header_name1, seq1, reg1, header_name2, seq2, reg2)
                return

            #if everything is fine, add sequence and print it to good file
            self.addGood(header_name1, seq1, reg1, header_name2, seq2, reg2)
            #sys.stdout.write('G')
            return

        else:
            #if everything is fine, add sequence and print it to good file
            #sys.stdout.write('g')
            self.addGood(header_name1, seq1, reg1, header_name2, seq2, reg2)
            return


    def printProgress(self):
        #print 'status',self.regcount, printevery,self.lastReg
        if int(self.regcount / printevery) != self.lastReg:
            sta = self.getStats()
            sys.stdout.write( '%s%14s registers; %14s reg/s avg; %14s reg/s last; %14s valid; %14s invalid; %6.2f%% invalid\n' %
                             (self.countstr,
                                "{:,}".format(sta ['# Number of registers']),
                                "{:,}".format(sta ['Speed since start']),
                                "{:,}".format(sta ['Speed last cicle']),
                                "{:,}".format(sta ['# Sequences']),
                                "{:,}".format(sta ['# Duplicated reads']),
                                sta ['% Duplicated reads'] ) )

    def getStats(self):
        regdiff       = self.regcount - self.lastcount
        lastcount     = self.regcount
        self.lastReg  = int(self.regcount / printevery)
        now           = time.time()
        elalast       = now - self.lasttime
        ela           = now - self.start
        speedlast     = int(     regdiff  / elalast)
        speed         = int(self.regcount / ela    )
        lendups       = len(self.dups)
        lenseqs       = len(self.seqs)
        if self.paired:
            lenseqs   = len(self.seqs)/2

        self.lasttime  = time.time()
        invalidPerc = (lendups / float(self.regcount) * 100)
        res = {
            'Now'                     : now,
            'Elapsed since last cicle': elalast,
            'Elapsed since start'     : ela,
            'Speed last cicle'        : speedlast,
            'Speed since start'       : speed,
            '# Duplicated reads'      : lendups,
            '% Duplicated reads'      : invalidPerc,
            '# Sequences'             : lenseqs,
            '# Number of registers'   : self.regcount
        }
        return res

    def report(self):
        print "Saving report %s:\n  " % self.countstr, self.report_fn

        if self.countstr != "":
            self.report_fhd.write( "%s\n"  % self.countstr                                                )

        #lenseqs      = len(self.seqs)
        #if self.paired:
            #lenseqs   = len(self.seqs)/2
        #lendups      = len(self.dups)

        self.report_fhd.write( "INPUT FILES : %s\n"  % " ".join([self.fn1, self.fn2])                     )
        for key, val in self.getStats().items():
            self.report_fhd.write( key.upper() + ": %d\n" % val )

        self.report_fhd.write( "\n=\n\n" )
        self.report_fhd.close()

        if not self.onlyid:
            print "Saving excluded list %s:\n  " % self.countstr, self.excluded_fn
            for duppos in range( 0, len( self.dups ), 10000 ):
                dup = self.dups[ duppos : duppos + 10000 ]
                self.excluded_fhd.writelines( [ x+"\n" for x in dup ] )
            self.excluded_fhd.close()

            print "Saving unique list %s:\n  " % self.countstr,
            for unipos in range( 0, len( self.uniqs ), 10000 ):
                uni = self.uniqs[ unipos : unipos + 10000 ]
                self.uniques_fhd.writelines( [ x+"\n" for c in uni ] )
            self.uniques_fhd.close()

        self.fhd1o.close()
        self.fhd1u.close()
        self.fhd2o.close()
        self.fhd2u.close()



class controller(object):
    def __init__(self):
        self._holders = []
        self._files   = {}
        self.num_open = -1

    def add(self, filename):
        print "adding %s to controller" % filename

        fhd              = None
        fhd_list         = None
        fhd_class        = None

        if filename in self._files:
            print "filename already in controller"
            print "opening reused file", filename
            holderId = self._files[ filename ]
            fhd              = self._holders[ holderId ][2]
            fhd_list         = self._holders[ holderId ][3]
            fhd_class        = self._holders[ holderId ][4]

        else:
            print "opening new file", filename
            mode = 'w'
            if os.path.exists(filename): mode = 'a'
            fhd              = openfile(filename, mode)
            fhd_list         = man.list()
            fhd_class        = filehandler( filename, fhd_list )

        self._holders.append( [ filename, True, fhd, fhd_list, fhd_class ] )

        self._files[ filename ] = len(self._holders) - 1

        #print fhd_class

        return fhd_class

    def _check(self):
        self.num_open = 0
        for data in self._holders:
            filename, opened, fhd, fhd_list, fhd_class = data
            #print "checking ", filename

            if opened:
                #print "checking ", filename, 'still open'
                self.num_open += 1

                fhdlen = len(fhd_list)
                if fhdlen > 0:
                    #print "writing to",filename,fhdlen,"registers"
                    fhd.writelines( fhd_list[:fhdlen-1] )
                    #print "written"

                    #print "deleting"
                    del fhd_list[:fhdlen-1]
                    #print "deleted"

            else:
                print "checking ", filename, 'already closed'
                pass

    def hasFinished(self):
        return self.num_open == 0

    def get_num_open(self):
        return self.num_open

    def close(self):
        for data in self._holders:
            filename, opened, fhd, fhd_list, fhd_class = data
            #print "checking ", filename

            if opened:
                print "checking ", filename, 'still open'
                fhd_class.close()

        self._check()

        while not self.hasFinished:
            self._check()

        for data in self._holders:
            filename, opened, fhd, fhd_list, fhd_class = data
            fhd.close()



def main():
    """
    Filters sequences for clonality.
    """
    cdir   = os.curdir
    parser = argparse.ArgumentParser(description='filter fastq files')
    parser.add_argument('-n', '--nsize'   ,                  dest='nsize'   , default=-1  , metavar='N', type=int  , nargs='?',                      help='number of bp to use [INT: default: -1 (all)]')
    parser.add_argument('-d', '--dir'     ,                  dest='dir'     , default=cdir, metavar='D', type=str  , nargs='?',                      help='output dir [STR: default: .]')
    parser.add_argument('-m', '--merge'   ,                  dest='merge'   , default=None, metavar='M', type=str  , nargs='?',                      help='merge output using prefix')
    parser.add_argument('-t', '--threaded',                  dest='threaded',                                                   action='store_true', help='Run multithreaded')
    parser.add_argument('-s', '--single'  ,                  dest='single'  ,                                                   action='store_true', help='treat as single end')
    parser.add_argument('-c', '--compress',                  dest='compress',                                                   action='store_true', help='compress in memory using md5')
    parser.add_argument('-i', '--only-ids',                  dest='onlyid'  ,                                                   action='store_true', help='only export id, no sequence')
    parser.add_argument('-o', '--homo'    , '--homopolymer', dest='homo'    ,                                                   action='store_true', help='compress homopolymers')
    parser.add_argument('-f', '--dry-run' ,                  dest='dryrun'  ,                                                   action='store_true', help='dry run')
    parser.add_argument('-z', '--gzip'    ,                  dest='gzip'    ,                                                   action='store_true', help='compress output to gzip')
    parser.add_argument('infiles',                                                          metavar='i',             nargs='+',                      help='input files')

    args     = parser.parse_args()
    nsize    = args.nsize
    dstdir   = args.dir
    single   = args.single
    merge    = args.merge
    compress = args.compress
    onlyid   = args.onlyid
    homopol  = args.homo
    zipfile  = args.gzip
    dry_run  = args.dryrun
    infiles  = args.infiles
    threaded = args.threaded

    dstdir   = os.path.abspath( dstdir )

    if nsize != -1: print "SHORTENING TO %dbp" % nsize
    if compress   : print "COMPRESSING IN MEMORY"
    if onlyid     : print "EXPORTING ONLY ID"
    if homopol    : print "COMPRESS HOMOPOLYMERS"
    if zipfile    : print "COMPRESSING OUTPUT FILE"


    numfiles = len(infiles)
    if (numfiles != 1) and (numfiles % 2 != 0):
        if not single:
            print "needs either to be 1 file, a multiple of 2 files or single (-s) reads"
            print "got", numfiles
            sys.exit( 1 )

    paired = False
    if ( numfiles % 2 == 0 ) and not single:
        paired = True




    if not os.path.exists( dstdir ):
        print "Destination dir %s does not exists. creating" % dstdir
        os.makedirs( dstdir )


    sffBack = []

    for filepos in range(len(infiles)):
        filename = infiles[ filepos ]
        if not os.path.exists( filename ):
            print "input file %s does not exists" % filename
            sys.exit( 1 )

        if filename.endswith('.sff'):
            outf               = filename.replace('.sff', '')
            outf               = os.path.basename( outf )
            outf               = os.path.join(     os.path.abspath(dstdir), outf )
            fileback           = sff2fq(           filename, outf )
            infiles[ filepos ] = fileback
            sffBack.append( [fileback, filename] )

        filename = infiles[ filepos ]
        if not os.path.exists( filename ):
            print "input file %s does not exists" % filename
            sys.exit( 1 )




    #TODO: MULTITHREADING
    #       CREATE A THREAD TO THE WRITTER IF MERGED
    #       CREATE A THREAD TO EACH ANALIZER
    #       CREATE A THREAD TO DATABASE WITH A QUEUE TO EACH ANALIZER


    #writterAll = writter()

    control = None
    if threaded:
        control = controller()

    cmds1   = []

    man_set  = None
    man_list = None
    if merge:
        print 'creating shared containers'
        ns.seqs      = set()
        man_set      = ns.seqs
        man_list_dup = man.list()
        man_list_uni = man.list()

    if paired:
        pairnum  = 0
        numpairs = numfiles / 2
        for filepos in range(0, numfiles, 2):
            pairnum += 1
            #analize(infiles[filepos], infiles[filepos+1], paired=paired,pairnum=pairnum,numpairs=numpairs,dstdir=dstdir,nsize=nsize,compress=compress,onlyid=onlyid,homopol=homopol,zipfile=zipfile,merge=merge,dry_run=dry_run)
            kwargs = { 'control': control, 'paired':paired, 'pairnum':pairnum, 'numpairs':numpairs, 'dstdir':dstdir, 'nsize':nsize, 'compress':compress, 'onlyid':onlyid, 'homopol':homopol, 'zipfile':zipfile, 'merge':merge, 'dry_run':dry_run }

            if merge:
                print "adding shared containers to "
                kwargs['seqs' ] = man_set
                kwargs['dups' ] = man_list_dup
                kwargs['uniqs'] = man_list_uni

            cmds1.append( [[infiles[filepos], infiles[filepos+1]], kwargs ] )
    else:
        filenum = 0
        for filepos in range(numfiles):
            filenum += 1
            #analize(infiles[filepos], ""                , paired=paired,filenum=filenum,numfiles=numfiles,dstdir=dstdir,nsize=nsize,compress=compress,onlyid=onlyid,homopol=homopol,zipfile=zipfile,merge=merge,dry_run=dry_run)
            kwargs = { 'control': control, 'paired':paired, 'pairnum':pairnum, 'numpairs':numpairs, 'dstdir':dstdir, 'nsize':nsize, 'compress':compress, 'onlyid':onlyid, 'homopol':homopol, 'zipfile':zipfile, 'merge':merge, 'dry_run':dry_run }
            cmds1.append( [[infiles[filepos], ""                ], kwargs] )



    cmds2 = []
    for cmd in cmds1:
        #print "PASSING COMMAND 1"
        args   = cmd[0]
        kwargs = cmd[1]
        #print "  ", args
        #print "  ", kwargs
        cmds2.append( analize( *args, **kwargs ) )


    if not threaded:
        for cmd in cmds2:
            #print "PASSING COMMAND 1"
            args   = cmd[0]
            kwargs = cmd[1]
            #print "  ", args
            #print "  ", kwargs
            analizeData( *args, **kwargs )

    else:
        multiprocessing.allow_connection_pickling()
        pool  = Pool(len(cmds2))
        procs = []
        for cmd in cmds2:
            if cmd is not None:
                args   = cmd[0]
                kwargs = cmd[1]
                print "PASSING COMMAND 2"
                #print "  ", args
                #print "  ", kwargs
                p = pool.apply_async( analizeData, args, kwargs )
                print "passed"
                procs.append( [ args[0], True, p ] )

        print "NOW WAITING"
        while True:
            still_running = False
            for procdata in procs:
                n, s, p = procdata
                if s:
                    try:
                        p.get(0.02)
                        procdata[1] = False
                        print "process", n, 'finished running'

                    except multiprocessing.TimeoutError:
                        #print "still running", n
                        still_running = True
                        pass

                    except:
                        except_type, except_class, tb = sys.exc_info()
                        print 'TYPE',except_type
                        print 'CLASS',except_class
                        print 'TRACE BACK',traceback.format_exc()
                        sys.exit(1)

            #print "should check"
            control._check()

            if not still_running:
                print "no more processes running. printing last lines"
                while not control.hasFinished:
                    control._check()

                break
        control.close()

    for fq, sff in sffBack:
        sffFilter(fq, sff)



def analize(fn1, fn2, control=None, paired=False,pairnum=None,numpairs=None,filenum=None,numfiles=None,dstdir='.',nsize=-1,compress=False,onlyid=False,homopol=False,zipfile=False,merge=False,dry_run=False,seqs=set(),dups=list(),uniqs=list()):
    #def openfile(infile, method, buff=64*1024*1024, compresslevel=1):
    fn1o         = ""
    fn1u         = ""

    fn2o         = ""
    fn2u         = ""

    good_files   = []
    bad_files    = []

    countstr = ""
    if paired and (None not in [pairnum, numpairs]):
        countstr = "%3d/%3d pairs " % (pairnum, numpairs)

    elif not paired and ( None not in [filenum, numfiles] ):
        countstr = "%3d/%3d files " % (filenum, numfiles)



    if not onlyid:
        fn1bn        = os.path.basename( fn1 )
        fn1dst       = os.path.join( dstdir, fn1bn )

        fn1dst       += '.rmdup'
        if merge is not None:
            fn1dst       += '.fwd'


        fn1o         = fn1dst + '.good.fastq'
        fn1u         = fn1dst + '.bad.fastq'

        if zipfile:
            fn1o += '.gz'
            fn1u += '.gz'

        mode = 'w'
        if merge is not None and os.path.exists(fn1o):
            mode = 'a'

        good_files   = [fn1o]
        bad_files    = [fn1u]



        if paired:
            fn2bn        = os.path.basename( fn2 )
            fn2dst       = os.path.join( dstdir, fn2bn )

            fn2dst       += '.rmdup'
            if merge is not None:
                fn2dst       += '.rev'

            fn2o         = fn2dst + '.good.fastq'
            fn2u         = fn2dst + '.bad.fastq'

            if zipfile:
                fn2o += '.gz'
                fn2u += '.gz'

            good_files.append(fn2o )
            bad_files.append( fn2u )




    print "Reading     %s:\n  "%countstr + "\n  ".join( [fn1, fn2] )
    if not onlyid:
        print "Saving good %s:\n  "%countstr + "\n  ".join( good_files )
        print "Saving bad  %s:\n  "%countstr + "\n  ".join( bad_files  )

    bn = fn1
    if fn2 != "":
        bn       = os.path.commonprefix([os.path.basename(x) for x in [fn1, fn2]])
    bn       = bn.strip('.').strip('_').strip('-')

    if merge is not None:
        bn = merge

    bn          = os.path.join( dstdir, bn )

    report_fn   = bn + '.rmdup.report'

    excluded_fn = bn + '.rmdup.excludedIds'
    uniques_fn  = bn + '.rmdup.uniqueIds'

    if zipfile:
        excluded_fn += '.gz'
        uniques_fn  += '.gz'

    print "Report     %s:\n  "%countstr + report_fn
    print "Excluded   %s:\n  "%countstr + excluded_fn
    print "Uniques    %s:\n  "%countstr + uniques_fn






    if not merge:
        print "cleaning sequences %s" % countstr
        seqs.clear()
        dups     = list()
        uniqs    = list()





    fhd1o        = None
    fhd1u        = None
    fhd2o        = None
    fhd2u        = None

    report_fhd   = None
    excluded_fhd = None
    uniques_fhd  = None

    repmode = 'w'
    if merge and os.path.exists( excluded_fn ):
        repmode = 'a'


    if control is None:
        fhd1o        = openfile(fn1o      , mode)
        fhd1u        = openfile(fn1u      , mode)

        if paired:
            fhd2o    = openfile(fn2o      , mode)
            fhd2u    = openfile(fn2u      , mode)

        report_fhd   = open(     report_fn   , repmode )
        excluded_fhd = openfile( excluded_fn , repmode )
        uniques_fhd  = openfile( uniques_fn  , repmode )

    else:
        fhd1o        = control.add(fn1o)
        fhd1u        = control.add(fn1u)

        if paired:
            fhd2o    = control.add(fn2o)
            fhd2u    = control.add(fn2u)

        report_fhd   = control.add( report_fn   )
        excluded_fhd = control.add( excluded_fn )
        uniques_fhd  = control.add( uniques_fn  )



    if dry_run:
        return None

    else:
        db_args   = [   fn1, fn1o, fn1u, fhd1o, fhd1u,
                        fn2, fn2o, fn2u, fhd2o, fhd2u,
                        report_fn  , report_fhd  ,
                        excluded_fn, excluded_fhd,
                        uniques_fn , uniques_fhd ,
                    ]

        db_kwargs = {
                        'countstr':countstr,
                        'pairnum' :pairnum , 'numpairs':numpairs,
                        'filenum' :filenum , 'numfiles':numfiles,
                        'paired'  :paired  , 'onlyid'  :onlyid  ,
                        'seqsl'   :seqs    , 'dupsl'   :dups    , 'uniqsl': uniqs
                    }



        db = seqdb(*db_args, **db_kwargs)

        an_kwargs  = {
            'nsize'   :nsize , 'compress':compress, 'homopol' :homopol,
        }

        return [[db], an_kwargs]





def loggerF(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        print "IN LOGGER"
        try:
            print "running logger"
            return f(*args, **kwargs)
        except:
            except_type, except_class, tb = sys.exc_info()
            print 'TYPE',except_type
            print 'CLASS',except_class
            print 'TRACE BACK',traceback.format_exc()
            raise
    return wrapper


@loggerF
def analizeData(    db , nsize=-1 , compress=False , homopol=False):

    print "RUNNING ANALYSIS IN DATA:", db.fn1


    fhd1i        = None
    fhd2i        = None

    fhd1i        = openfile(db.fn1, 'r')

    if db.paired:
        fhd2i    = openfile(db.fn2, 'r')


    EOF = False
    regcount = 0
    while True:
        if DEBUG and regcount == DEBUG_NUM_READS:
            break
        regcount += 1

        reg1      = list(islice( fhd1i, 4 ))
        reg2      = None
        if db.paired:
            reg2      = list(islice( fhd2i, 4 ))

        if len( reg1 ) == 0:
            break

        if db.paired and len( reg2 ) == 0:
            break

        #strips the /1 /2 at the end
        header_name1 = reg1[0].strip()[1:]
        header_name1 = rep1.sub('', header_name1)

        #checks forward strand
        #strip new line
        seq1s   = reg1[1].strip()

        #cut if requested
        if nsize != -1:
            seq1s   = seq1s[:nsize]

        # colapse homopolymers
        if homopol:
            seq1s = rehomo1.sub(rehomo2, seq1s)

        # create reverse complement
        seq1src = seq1s.translate( trans )[::-1]

        # find lexicaly smaller sequence
        seq1min = min([seq1s, seq1src])

        # compress if requested
        if compress:
            seq1min = hashlib.md5(seq1min).digest()
            #seq1min = zlib.compress(seq1min)


        #check reverse strand
        if db.paired:
            header_name2 = reg2[0].strip()[1:]
            header_name2 = rep2.sub('', header_name2)

            #check if still in sync
            if header_name1 != header_name2:
                print "out of sync"
                print header_name1
                print header_name2
                sys.exit( 1 )


            seq2s   = reg2[1].strip()
            if nsize != -1:
                seq2s   = seq2s[:nsize]

            if homopol:
                seq2s = rehomo1.sub(rehomo2, seq2s)

            seq2src = seq2s.translate( trans )[::-1]

            seq2min = min([seq2s, seq2src])
            if compress:
                seq2min = hashlib.md5(seq2min).digest()
                #seq2min = zlib.compress(seq2min)


            # check for palindrome or repeats
            lenseqs = len(set([seq1s, seq1src, seq2s, seq2src]))

            if lenseqs != 4:
                db.add( header_name1, seq1min, reg1, header_name2, seq2min, reg2, bad=True )
                continue

            db.add( header_name1, seq1min, reg1, header_name2, seq2min, reg2 )

        else:
            # check for palindrome or repeats
            lenseqs = len(set([seq1s, seq1src]))

            if lenseqs != 2:
                db.add( header_name1, seq1min, reg1, header_name2, seq2min, reg2, bad=True )
                continue

            #if everything is fine, add sequence and print it to good file
            db.add( header_name1, seq1min, reg1, header_name2, seq2min, reg2 )

    db.report()


if __name__ == '__main__': main()
